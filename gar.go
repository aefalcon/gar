package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"syscall"
)

func Usage() {
	cmdName := os.Args[0]
	fmt.Fprintf(os.Stderr, "Usage of %s:\n\n", cmdName)
	fmt.Fprintf(os.Stderr, "  %s [OPTIONS] [--] PACKAGE [ARGS...]\n\n", cmdName)
	fmt.Fprintf(os.Stderr, "OPIONS:\n")
	flag.PrintDefaults()
}

var debugMode = flag.Bool("debug", false, "debug mode")
var getBeforeExec = flag.Bool("get", true, "`go get` before running")

func DebugPrint(v ...interface{}) {
	if *debugMode {
		log.Print(v...)
	}
}

func DebugPrintf(format string, v ...interface{}) {
	if *debugMode {
		log.Printf(format, v...)
	}
}

func GetPackage(pkgName string) {
	DebugPrint("getting and updating packaging and dependencies")
	cmd := exec.Command("go", "get", "-u", pkgName)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatalf("failed to go get package: %s", err)
	}
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	args := flag.Args()
	if nargs := len(args); nargs < 1 {
		DebugPrint("not enough arguments")
	}
	pkgName := args[0]
	DebugPrintf("package to run is %s", pkgName)
	if *getBeforeExec {
		GetPackage(pkgName)
	}
	workDir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatalf("unable to create temporary directory: %s", err)
	}
	execPath := filepath.Join(workDir, "exec")
	DebugPrintf("exec output path is %s", execPath)
	cmd := exec.Command("go", "build", "-o", execPath, pkgName)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatalf("failed to build: %s", err)
	}
	execFile, err := os.Open(execPath)
	if err != nil {
		log.Fatalf("unable to open executable: %s", err)
	}
	err = os.RemoveAll(workDir)
	if err != nil {
		log.Fatalf("failed to remove work directory: %s", err)
	}
	pid := os.Getpid()
	pidstr := strconv.Itoa(pid)
	fdstr := strconv.Itoa(int(execFile.Fd()))
	execPath = filepath.Join(string(filepath.Separator), "proc", pidstr, "fd", fdstr)
	DebugPrintf("exec path is %s", execPath)
	err = syscall.Exec(execPath, args, []string{})
	if err != nil {
		log.Fatalf("failed to exec: %s", err)
	}
}
